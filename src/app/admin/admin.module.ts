import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AboutPageManagementComponent } from './views/pages/about-page-management/about-page-management.component';
import { ContactPageManagementComponent } from './views/pages/contact-page-management/contact-page-management.component';


@NgModule({
  declarations: [AboutPageManagementComponent, ContactPageManagementComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
