import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { ShareComponentComponent } from './components/share-component/share-component.component';
import { ShareDirectiveDirective } from './directives/share-directive.directive';


@NgModule({
  declarations: [ShareComponentComponent, ShareDirectiveDirective],
  imports: [
    CommonModule,
    SharedRoutingModule
  ]
})
export class SharedModule { }
