import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { AboutPageComponent } from './views/pages/about-page/about-page.component';
import { ContactPageComponent } from './views/pages/contact-page/contact-page.component';
import { ServicePageComponent } from './views/pages/service-page/service-page.component';
import { CareerPageComponent } from './views/pages/career-page/career-page.component';
import { CareerApplyPageComponent } from './views/pages/career-apply-page/career-apply-page.component';
import { HomePageComponent } from './views/pages/home-page/home-page.component';
import { ProductPageComponent } from './views/pages/product-page/product-page.component';
import { SharedModule } from '../client/views/shared/shared.module';


@NgModule({
  declarations: [AboutPageComponent, ContactPageComponent, ServicePageComponent, CareerPageComponent, CareerApplyPageComponent, HomePageComponent, ProductPageComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
    SharedModule
  ]
})
export class ClientModule { }
