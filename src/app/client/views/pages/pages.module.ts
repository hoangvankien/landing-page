import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module'
import { AboutPageComponent } from './about-page/about-page.component'


@NgModule({
  declarations: [
    AboutPageComponent,

  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class PagesModule { }
