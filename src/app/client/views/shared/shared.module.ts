import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { HeaderComponentComponent } from './components/header-component/header-component.component';
import { FooterComponentComponent } from './components/footer-component/footer-component.component';


@NgModule({
  declarations: [HeaderComponentComponent, FooterComponentComponent],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports: [HeaderComponentComponent, FooterComponentComponent]
})
export class SharedModule { }
